package advent.of.code.twenty;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {

    public static long crunch(String s) {
        List<Long> pool = Stream.of(s.split("\n"))
            .map(Long::parseLong)
            .collect(Collectors.toList());
        for (Long a : pool) {
            for (Long b : pool) {
                if (a + b == 2020) {
                    return a * b;
                }
            }
        }
        throw new RuntimeException("No suitable combination found");
    }

    public static long crunch3(String s) {
        List<Long> pool = Stream.of(s.split("\n"))
            .map(Long::parseLong)
            .collect(Collectors.toList());
        for (Long a : pool) {
            for (Long b : pool) {
                for (Long c : pool) {
                    if (a + b + c == 2020) {
                        return a * b * c;
                    }
                }
            }
        }
        throw new RuntimeException("No suitable combination found");
    }
}
