package advent.of.code.twenty;

import java.util.stream.Stream;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PwPolicy {

    private final int min;
    private final int max;
    private final String subStr;
    private final String password;

    public static PwPolicy parse(String line) {
        final PwPolicy build = PwPolicy.builder()
            .password(line.split(":")[1].trim())
            .min(Integer.parseInt(line.split("-")[0].trim()))
            .max(Integer.parseInt(line.split(" ")[0].split("-")[1].trim()))
            .subStr(line.split(":")[0].split(" ")[1].trim())
            .build();
        String check = build.toString();
        if (!check.equals(line)) {
            System.out.println(line + " doesnt match " + check);
        }
        return build;
    }

    public static long findNumberOfValidPws(String input) {
        return Stream.of(input.split("\n"))
            .map(PwPolicy::parse)
            .filter(PwPolicy::isValid)
            .count();
    }

    public static long findNumberOfValidPwsTobogan(String input) {
        return Stream.of(input.split("\n"))
            .map(PwPolicy::parse)
            .filter(PwPolicy::isValidTobogan)
            .count();
    }

    private boolean isValid() {
        int numMatches = 0;
        String currentString = password;
        while (currentString.contains(subStr)) {
            currentString = currentString.substring(currentString.indexOf(subStr) + subStr.length());
            numMatches++;
            if (numMatches > max) {
                return false;
            }
        }
        if (numMatches < min) {
            return false;
        }
        return true;
    }

    private boolean isValidTobogan() {
        final boolean firstPositionMatches = password.substring(min - 1, min).equals(subStr);
        final boolean secondPositionMatches = password.substring(max - 1, max).equals(subStr);

        return firstPositionMatches ^ secondPositionMatches;
    }

    public String toString() {
        return min + "-" + max + " " + subStr + ": " + password;
    }
}
